POS PROJECT
-
Adalah project yang di buat untuk keperluan rekrutmen ******PT ERPORATE SOLUSI GLOBAL******, dibangun dengan bahasa pemrograman PHP, menggunakan framework laravel versi terbaru, dan database mysql, silahkan ikuti langkah dibawah untuk proses instalasi.

Installation:
-
- Saya asumsikan php, composer dan mysql sudah terinstall dan berjalan dengan baik, minimum version yang digunakan adalah php versi 7.1
- clone project melalui link git yang sudah dilampirkan
- copy file .env.example menjadi .env
- buka terminal dan ketik "composer install" pada root diretory project
- sesuaikan setting database anda di file konfigurasi tersebut
- integrasikan database, bisa menggunakan file "dump.sql" yang sudah terlampir, atau menggunakan perintah "php artisan migrate --seed"
- jalankan server php dengan perintah "php artisan serve"
- jalankan "php artisan jwt:secret" agar token bisa bekerja
- jalankan browser dan kunjungi localhost:8000

Level User:
-
- Admin (user: admin@pos.com, pass: password)
- Kasir (user: kasir@pos.com, pass: password)
- Pelayan (user: pelayan@pos.com, pass: password)

Module:
-
- Dashboard
- Product (crud)
- Category (crud)
- Order (crud)
- Employee (crud)
- Log Activity (readonly)

Ketangan:
-
- ****Admin**** bisa melakukan semua akses.
- ****Kasir**** memiliki akses antara lain: dashboard, melihat produk yang tersedia, melakukan order(crud), menyelesaikan order dan pembayaran.
- ****Pelayan**** memiliki akses antara lain: dashboard, melihat produk yang tersedia, melakukan order(crud).

API:
-

****login**** /login
- digunakan untuk mendapatkan token.
- parameter: email, password

****logout**** /logout
- digunakan untuk menonaktifkan token
- memerlukan token

****refresh**** /refresh
- digunakan untuk refresh token
- memerlukan token.

****me**** /me
- digunakan untuk mendapatkan profile user aktif
- memerlukan token

****product**** /product
- digunakan untuk mendapatkan data produk.
- memerlukan token
