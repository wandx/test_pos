<?php

namespace App\Http\Controllers;

use App\Models\ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Yajra\DataTables\Facades\DataTables;

class CategoryCont extends Controller
{
    public function index(){
        return view("category.index");
    }

    public function data(ProductCategory $category){
        return DataTables::of($category->newQuery())
            ->addColumn("action",function($model){
                if(auth()->user()->role == "admin"){
                    $li  = "<button data-toggle='modal' data-target='#edit-category' data-id='{$model->id}' class='btn btn-info btn-xs'><i class='fa fa-pencil'></i></button>";
                    $li .= "<button class='btn btn-danger btn-xs' onclick='deleteCategory({$model->id})'><i class='fa fa-trash'></i></button>";
                    return $li;
                }
                return "";

            })
            ->make(true);
    }

    public function store(Request $request,ProductCategory $category){
        $validator = app("validator")->make($request->all(),[
            "name" => "required"
        ]);

        if($validator->fails()){
            return response()->json([
                "_token" => csrf_token(),
                "messages" => $validator->messages()
            ],403);
        }
        $request->merge(["slug"=>Str::slug($request->input("name"))]);

        $store = $category->newQuery()->create($request->except(["_token"]));

        set_activity("Menambahkan kategori bernama ".$store->name);

        return response()->json([
            "_token" => csrf_token()
        ]);
    }

    public function update(Request $request, ProductCategory $category){
        $u = $category->newQuery()->findOrFail($request->input("id"));
        $name = $u->name;
        $validator = app("validator")->make($request->all(),[
            "name" => "required",
        ]);

        if($validator->fails()){
            return response()->json([
                "_token" => csrf_token(),
                "messages" => $validator->messages()
            ],403);
        }

        $request->merge(["slug"=>Str::slug($request->input("name"))]);
        $u->update($request->except(["_token","id"]));

        set_activity("Update kategori bernama ".$name);

        return response()->json([
            "_token" => csrf_token()
        ]);
    }

    public function destroy($id,ProductCategory $category){
        $u = $category->newQuery()->findOrFail($id);
        $name = $u->name;
        $u->delete();
        set_activity("Menghapus kategori bernama ".$name);
        return response()->json(["message"=>"ok"]);
    }

    public function fetch($id,ProductCategory $category){
        return response()->json($category->newQuery()->findOrFail($id));
    }
}
