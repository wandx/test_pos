<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class OrderCont extends Controller
{
    public function index(){
        return view("order.index");
    }

    public function data(Order $order){
        if(auth()->user()->role == "admin"){
            $q = $order->newQuery()->with("user");
        }else{
            $q = $order->newQuery()->where("status","active")->with("user");
        }

        return DataTables::of($q)
                ->addColumn("action",function($model){
                    $li = "<button data-toggle='modal' data-target='#order-detail' data-id='{$model->id}' class='btn btn-info btn-xs'><i class='fa fa-eye'></i></button>";
                    $li .= "<button class='btn btn-danger btn-xs' onclick='deleteOrder({$model->id})'><i class='fa fa-trash'></i></button>";

                    if(auth()->user()->role != "pelayan" && $model->status != "done"){
                        $li .= "<button data-toggle='modal' data-target='#payment' data-id='{$model->id}' data-code='{$model->code}' data-total='{$model->total}' class='btn btn-success btn-xs'><i class='fa fa-check'></i></button>";
                    }

                    return $li;
                })
                ->make(true);
    }

    public function dataProduct(Product $product){
        $q = $product->newQuery()->whereIsReady(1)->with("product_category");

        return DataTables::of($q)
            ->addColumn("action",function ($model){
                $li  = "<button onclick='addItem(\"{$model->name}\",{$model->price},{$model->id})' class='btn btn-info btn-xs' type='button'><i class='fa fa-plus'></i></button>";
                return $li;

            })
            ->editColumn("is_ready",function($model){
                return $model->is_ready == 1 ? "YA":"TIDAK";
            })
            ->make(true);
    }

    public function store(Request $request, Order $order){
        $code = $this->buildCode();

        $store = $order->newQuery()->create([
            "code" => $code,
            "status" => "active",
            "table_number" => $request->input("table_number"),
            "user_id" => auth()->user()->id,
            "total" => $request->input("total")
        ]);

        $total = 0;
        foreach ($request->input("pid") as $k=>$pid){
            foreach ($request->input("pname_input") as $k2=>$pname){
                foreach ($request->input("price_input") as $k3=>$price){
                    foreach ($request->input("qty") as $k4=>$qty){
                        if($k == $k2 && $k2 == $k3 && $k3 == $k4){
                            $store->order_items()->create([
                                "name" => $pname,
                                "qty" => $qty,
                                "sub_total" => $price*$qty,
                            ]);

                            $total += ($price*$qty);
                        }
                    }
                }
            }
        }

        set_activity("Membuat order dengan code ".$code);
        return response()->json([
            "_token" => csrf_token(),
        ]);
    }

    public function orderDetail($id,Order $order){
        $order = $order->newQuery()->findOrFail($id);
        return view("order.detail",compact("order"));
    }

    public function update(){}

    public function destroy($id, Order $order){
        $order = $order->newQuery()->findOrFail($id);
        set_activity("Hapus order dengan code ".$order->code);
        $order->order_items()->delete();
        $order->delete();
        return response()->json(["message"=>"ok"]);
    }

    public function fetch(){}

    public function pay(Request $request,Order $order){
        $id = $request->input("id");

        $o = $order->newQuery()->find($id);
        $code = $o->code;

        $o->update([
            "payment_amount" => $request->input("payment_amount"),
            "status" => "done"
        ]);
        set_activity("Memproses pembayaran dengan code ".$code);

        return response()->json([
            "_token" => csrf_token()
        ]);


    }

    private function buildCode(){
        return "ERP".Carbon::now()->format("dmY").rand(100,9999);
    }
}
