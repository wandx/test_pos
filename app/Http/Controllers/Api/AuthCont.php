<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthCont extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

    public function login()
    {
        $credentials = request(['email', 'password']);

        if (! $token = auth("api")->attempt($credentials)) {
            return response()->json([
                "code" => 401,
                "message" => "Unauthorized"
            ], 401);
        }

        return $this->respondWithToken($token);
    }

    public function me()
    {
        return response()->json([
            "code" => 200,
            "message" => "OK",
            "data" => auth("api")->user()
        ]);
    }

    public function logout()
    {
        auth("api")->logout();

        return response()->json([
            "code" => 200,
            "message" => "OK"
        ]);
    }

    public function refresh()
    {
        return $this->respondWithToken(auth("api")->refresh());
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            "code" => 200,
            "message" => "ok",
            "data"=>[
                "token" => $token
            ]
        ]);
    }
}
