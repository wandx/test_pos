<?php

namespace App\Http\Controllers\Api;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductCont extends Controller
{
    public function index(Product $product){
        $product = $product->newQuery();

        if($product->count() > 0){
            return response()->json([
                "code" => 200,
                "message" => "OK",
                "data" => $product->with("product_category")->get()
            ]);
        }

        return response()->json([
            "code" => 404,
            "message" => "Not found",
        ],404);
    }
}
