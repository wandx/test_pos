<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LoginCont extends Controller
{
    public function index(){
        return view("login");
    }

    public function doLogin(Request $request){
        $validator = app("validator")->make($request->all(),[
            "email" => "required|email",
            "password" => "required"
        ]);

        if($validator->fails()){
            return back()->withErrors($validator->messages())->withInput();
        }

        if(!auth()->attempt($request->only(["email","password"]),$request->filled("remember"))){
            return back()->withErrors(["failed"=>"Kombinasi Email dan password belum benar"])->withInput();
        }

        return redirect()->route("dashboard")->withSuccess("Selamat datang ".auth()->user()->name);
    }

    public function doLogout(){
        auth()->logout();
        return redirect("/")->withSuccess("Anda berhasil logout.");
    }
}
