<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductCategory;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class ProductCont extends Controller
{
    public function index(ProductCategory $category){
        $category = $category->newQuery()->pluck("name","id");
        return view("product.index",compact("category"));
    }

    public function data(Product $product){
        if(auth()->user()->role == "admin"){
            $q = $product->newQuery()->with("product_category");
        }else{
            $q = $product->newQuery()->whereIsReady(1)->with("product_category");
        }

        return DataTables::of($q)
                    ->addColumn("action",function ($model){
                        if(auth()->user()->role == "admin"){
                            $li  = "<button data-toggle='modal' data-target='#edit-product' data-id='{$model->id}' class='btn btn-info btn-xs'><i class='fa fa-pencil'></i></button>";
                            $li .= "<button class='btn btn-danger btn-xs' onclick='deleteProduct({$model->id})'><i class='fa fa-trash'></i></button>";
                            return $li;
                        }
                        return "";

                    })
                    ->editColumn("is_ready",function($model){
                        return $model->is_ready == 1 ? "YA":"TIDAK";
                    })
                    ->make(true);
    }

    public function store(Request $request, Product $product){
        $validator = app("validator")->make($request->all(),[
            "name" => "required",
            "product_category_id" => "required",
            "description" => "required",
            "is_ready" => "required",
            "price" => "required|numeric"
        ]);

        if($validator->fails()){
            return response()->json([
                "_token" => csrf_token(),
                "messages" => $validator->messages()
            ],403);
        }

        $store = $product->newQuery()->create($request->except("_token"));
        set_activity("Menambahkan produk bernama ".$store->name);
        return response()->json([
            "_token" => csrf_token()
        ]);
    }

    public function update(Request $request,Product $product){
        $p = $product->newQuery()->findOrFail($request->input("id"));
        $name = $p->name;
        $validator = app("validator")->make($request->all(),[
            "name" => "required",
            "product_category_id" => "required",
            "description" => "required",
            "is_ready" => "required",
            "price" => "required|numeric"
        ]);

        if($validator->fails()){
            return response()->json([
                "_token" => csrf_token(),
                "messages" => $validator->messages()
            ],403);
        }

        $p->update($request->except("_token","id"));
        set_activity("Melakukan perubahan pada produk bernama ".$name);
        return response()->json([
            "_token" => csrf_token()
        ]);
    }

    public function destroy($id,Product $product){
        $product->newQuery()->delete();
        return response()->json([
            "message" => "ok"
        ]);
    }

    public function fetch($id,Product $product){
        $x = $product->newQuery()->findOrFail($id);

        return response()->json($x);
    }
}
