<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\User;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class ActivityCont extends Controller
{
    public function index(){
        return view("activity");
    }

    public function data(Activity $activity){
        return DataTables::of($activity->newQuery()->with("user"))
                ->make(true);
    }
}
