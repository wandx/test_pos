<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class EmployeeCont extends Controller
{
    public function index(){
        return view("employee.index");
    }

    public function data(User $user){
        return DataTables::of($user->newQuery()->where("role","!=","admin"))
                ->addColumn("action",function($model){
                    $li  = "<button data-toggle='modal' data-target='#edit-user' data-id='{$model->id}' class='btn btn-info btn-xs'><i class='fa fa-pencil'></i></button>";
                    $li .= "<button class='btn btn-danger btn-xs' onclick='deleteUser({$model->id})'><i class='fa fa-trash'></i></button>";
                    return $li;
                })
                ->make(true);
    }

    public function store(Request $request,User $user){
        $validator = app("validator")->make($request->all(),[
            "name" => "required",
            "email" => "required|email|unique:users,email",
            "password" => "required|confirmed",
            "role" => "required"
        ]);

        if($validator->fails()){
            return response()->json([
                "_token" => csrf_token(),
                "messages" => $validator->messages()
            ],403);
        }

        $request->merge(["password"=>bcrypt($request->input("password"))]);
        $store = $user->newQuery()->create($request->except(["_token","password_confirmation"]));

        set_activity("Menambahkan pegawai bernama ".$store->name);

        return response()->json([
            "_token" => csrf_token()
        ]);
    }

    public function update(Request $request, User $user){
        $u = $user->newQuery()->findOrFail($request->input("id"));
        $name = $u->name;
        $validator = app("validator")->make($request->all(),[
            "name" => "required",
            "email" => "sometimes|email",
            "role" => "required"
        ]);

        if($validator->fails()){
            return response()->json([
                "_token" => csrf_token(),
                "messages" => $validator->messages()
            ],403);
        }

        $request->offsetUnset("password");

        if($request->has("password")){
            $request->merge(["password"=>bcrypt($request->input("password"))]);
        }

        $u->update($request->except(["_token","password_confirmation","id"]));

        set_activity("Update pegawai bernama ".$name);

        return response()->json([
            "_token" => csrf_token()
        ]);
    }

    public function destroy($id,User $user){
        $u = $user->newQuery()->findOrFail($id);
        $name = $u->name;
        $u->delete();
        set_activity("Menghapus pegawai bernama ".$name);
        return response()->json(["message"=>"ok"]);
    }

    public function fetch($id,User $user){
        return response()->json($user->newQuery()->findOrFail($id));
    }
}
