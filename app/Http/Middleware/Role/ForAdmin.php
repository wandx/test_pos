<?php

namespace App\Http\Middleware\Role;

use Closure;

class ForAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->role == "admin"){
            return $next($request);
        }
        return abort(403);
    }
}
