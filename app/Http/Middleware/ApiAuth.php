<?php

namespace App\Http\Middleware;

use Closure;

class ApiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth("api")->check()){
            return $next($request);
        }
        return response()->json([
            "code" => 401,
            "message" => "Unauthorized"
        ],401);

    }
}
