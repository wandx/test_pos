<?php

function set_activity($activity){
    (new \App\Models\Activity())->newQuery()->create([
        "desc" => $activity,
        "user_id" => auth()->user()->id
    ]);
}
