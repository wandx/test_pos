<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("users")->truncate();
        DB::table("users")->insert([
            [
                "name" => "Admin",
                "email" => "admin@pos.com",
                "password" => bcrypt("password"),
                "role" => "admin",
            ],
            [
                "name" => "Kasir",
                "email" => "kasir@pos.com",
                "password" => bcrypt("password"),
                "role" => "kasir",
            ],
            [
                "name" => "Pelayan",
                "email" => "pelayan@pos.com",
                "password" => bcrypt("password"),
                "role" => "pelayan",
            ]
        ]);
    }
}
