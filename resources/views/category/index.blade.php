@extends("master")

@section("page_header")
    Kategori
    @if(auth()->user()->role == "admin")
    <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#add-category">Tambah Kategori</button>
    @endif
@endsection

@push("contents")
    <div class="panel">
        <div class="panel-body">
            <table class="table" id="dtable">
                <thead>
                <tr>
                    <th>Nama</th>
                    <th>Slug</th>
                    <th>Aksi</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@endpush

@push("contents")
    <!-- Modal -->
    <div id="add-category" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <form method="post" class="modal-content" id="f-add-category" onsubmit="submitAddCategory(event)">
                {!! csrf_field() !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah Kategori</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Nama</label>
                        <input type="text" name="name" id="name" required class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary" type="submit">Submit</button>
                </div>
            </form>

        </div>
    </div>

@endpush

@push("contents")
    <!-- Modal -->
    <div id="edit-category" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <form method="post" class="modal-content" id="f-edit-category" onsubmit="submitEditCategory(event)">
                {!! csrf_field() !!}
                <input type="hidden" name="id" id="id">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit User</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name2">Nama</label>
                        <input type="text" name="name" id="name2" required class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary" type="submit">Submit</button>
                </div>
            </form>

        </div>
    </div>

@endpush


@push("scripts")
    <script>
        var dtable;
        var formAddCategory = $("#f-add-category");
        var modalAddCategory = $("#add-category");
        var formEditCategory = $("#f-edit-category");
        var modalEditCategory = $("#edit-category");

        $(function(){
            dtable = $("#dtable").DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route("category.data") }}",
                columns:[
                    { data: "name",name: "name"},
                    { data: "slug",name: "slug"},
                    { data: "action",name: "action",orderable:false,searchable:false},
                ]
            });
        });

        function submitAddCategory(e){
            e.preventDefault();
            var data = formAddCategory.serialize();

            $.when($.ajax({
                url: "{{ route("category.store") }}",
                dataType: "JSON",
                type: "POST",
                data: data,
                success: function(resp){
                    // update token
                    modalAddCategory.modal("hide");
                    formAddCategory.trigger("reset");
                    formAddCategory.find("input[name=_token]").val(resp._token);
                    dtable.ajax.reload();
                    toastr.success("Berhasil menambah kategori.");

                },
                error: function(resp){
                    var messages = resp.responseJSON.messages;
                    var token = resp.responseJSON._token;

                    // update token
                    formAddCategory.find("input[name=_token]").val(token);

                    if(typeof messages.name === 'object'){
                        toastr.error(messages.name[0]);
                    }
                }
            }));
        }

        function submitEditCategory(e){
            e.preventDefault();
            var data = formEditCategory.serialize();

            $.when($.ajax({
                url: "{{ route("category.update") }}",
                dataType: "JSON",
                type: "POST",
                data: data,
                success: function(resp){
                    // update token
                    modalEditCategory.modal("hide");
                    formEditCategory.trigger("reset");
                    formEditCategory.find("input[name=_token]").val(resp._token);
                    dtable.ajax.reload();
                    toastr.success("Berhasil update kategori.");

                },
                error: function(resp){
                    var messages = resp.responseJSON.messages;
                    var token = resp.responseJSON._token;

                    // update token
                    formEditCategory.find("input[name=_token]").val(token);

                    if(typeof messages.name === 'object'){
                        toastr.error(messages.name[0]);
                    }
                }
            }));
        }
        
        function deleteCategory(id) {
            var c = confirm("Hapus Kategori ?");

            if(c){
                $.ajax({
                    url: "/kategori/"+id+"/hapus",
                    dataType: "JSON",
                    type: "GET",
                    success:function(resp){
                        toastr.success("Kategori dihapus");
                        dtable.ajax.reload();
                    },error(resp){
                        toastr.error("Terjadi kesalahan");
                    }
                })
            }
        }

        modalEditCategory.on("show.bs.modal",function(e){
            var trig = $(e.relatedTarget);
            var modal = $(this);
            var id = trig.data("id");

            $.getJSON("/kategori/"+id+"/fetch",function(resp){
               modal.find("#name2").val(resp.name);
               modal.find("#email2").val(resp.email);
               modal.find("#role2").val(resp.role);
               modal.find("#id").val(resp.id);
            });
        })


    </script>
@endpush
