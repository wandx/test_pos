@extends("master")

@section("page_header")
    Daftar Produk
    @if(auth()->user()->role == "admin")
    <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#add-product">Tambah Produk</button>
    @endif
@endsection

@push("contents")
    <div class="panel">
        <div class="panel-body">
            <table class="table" id="dtable">
                <thead>
                <tr>
                    <th>Nama</th>
                    <th>Price</th>
                    <th>Ready?</th>
                    <th>Desc</th>
                    <th>Kategori</th>
                    <th>Aksi</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@endpush

@push("contents")
    <!-- Modal -->
    <div id="add-product" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <form method="post" class="modal-content" id="f-add-product" onsubmit="submitAddProduct(event)">
                {!! csrf_field() !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah Produk</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Nama</label>
                        <input type="text" name="name" id="name" required class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="price">Price</label>
                        <input type="number" name="price" id="price" required class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="ready">Ready?</label>
                        <select name="is_ready" id="ready" class="form-control">
                            <option value="">Product ready?</option>
                            <option value="1">Ya</option>
                            <option value="0">Tidak</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="desc">Deskripsi</label>
                        <input type="text" name="description" id="desc" required class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="kategori">Kategori</label>
                        <select name="product_category_id" id="kategori" class="form-control">
                            <option value="">Pilih Kategori</option>
                            @foreach($category as $id=>$name)
                                <option value="{{ $id }}">{{ $name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary" type="submit">Submit</button>
                </div>
            </form>

        </div>
    </div>

@endpush

@push("contents")
    <!-- Modal -->
    <div id="edit-product" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <form method="post" class="modal-content" id="f-edit-product" onsubmit="submitEditProduct(event)">
                {!! csrf_field() !!}
                <input type="hidden" name="id" id="id">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Produk</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name2">Nama</label>
                        <input type="text" name="name" id="name2" required class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="price2">Price</label>
                        <input type="number" name="price" id="price2" required class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="ready2">Ready?</label>
                        <select name="is_ready" id="ready2" class="form-control">
                            <option value="">Product ready?</option>
                            <option value="1">Ya</option>
                            <option value="0">Tidak</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="desc2">Deskripsi</label>
                        <input type="text" name="description" id="desc2" required class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="kategori2">Kategori</label>
                        <select name="product_category_id" id="kategori2" class="form-control">
                            <option value="">Pilih Kategori</option>
                            @foreach($category as $id=>$name)
                                <option value="{{ $id }}">{{ $name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary" type="submit">Submit</button>
                </div>
            </form>

        </div>
    </div>

@endpush


@push("scripts")
    <script>
        var dtable;
        var formAddProduct = $("#f-add-product");
        var modalAddProduct = $("#add-product");
        var formEditProduct = $("#f-edit-product");
        var modalEditProduct = $("#edit-product");

        $(function(){
            dtable = $("#dtable").DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route("product.data") }}",
                columns:[
                    { data: "name",name: "name"},
                    { data: "price",name: "price"},
                    { data: "is_ready",name: "is_ready"},
                    { data: "description",name: "description"},
                    { data: "product_category.name",name: "product_category.name",orderable:false,searchable:false},
                    { data: "action",name: "action",orderable:false,searchable:false},
                ]
            });
        });

        function submitAddProduct(e){
            e.preventDefault();
            var data = formAddProduct.serialize();

            $.when($.ajax({
                url: "{{ route("product.store") }}",
                dataType: "JSON",
                type: "POST",
                data: data,
                success: function(resp){
                    // update token
                    modalAddProduct.modal("hide");
                    formAddProduct.trigger("reset");
                    formAddProduct.find("input[name=_token]").val(resp._token);
                    dtable.ajax.reload();
                    toastr.success("Berhasil menambah produk.");

                },
                error: function(resp){
                    var messages = resp.responseJSON.messages;
                    var token = resp.responseJSON._token;

                    // update token
                    formAddProduct.find("input[name=_token]").val(token);


                    if(typeof messages.product_category_id === 'object'){
                        toastr.error(messages.product_category_id[0]);
                    }

                    if(typeof messages.price === 'object'){
                        toastr.error(messages.price[0]);
                    }

                    if(typeof messages.name === 'object'){
                        toastr.error(messages.name[0]);
                    }

                    if(typeof messages.is_ready === 'object'){
                        toastr.error(messages.is_ready[0]);
                    }

                    if(typeof messages.description === 'object'){
                        toastr.error(messages.description[0]);
                    }
                }
            }));
        }

        function submitEditProduct(e){
            e.preventDefault();
            var data = formEditProduct.serialize();

            $.when($.ajax({
                url: "{{ route("product.update") }}",
                dataType: "JSON",
                type: "POST",
                data: data,
                success: function(resp){
                    // update token
                    modalEditProduct.modal("hide");
                    formEditProduct.trigger("reset");
                    formEditProduct.find("input[name=_token]").val(resp._token);
                    dtable.ajax.reload();
                    toastr.success("Berhasil update produk.");

                },
                error: function(resp){
                    var messages = resp.responseJSON.messages;
                    var token = resp.responseJSON._token;

                    // update token
                    formEditProduct.find("input[name=_token]").val(token);


                    if(typeof messages.product_category_id === 'object'){
                        toastr.error(messages.product_category_id[0]);
                    }

                    if(typeof messages.price === 'object'){
                        toastr.error(messages.price[0]);
                    }

                    if(typeof messages.name === 'object'){
                        toastr.error(messages.name[0]);
                    }

                    if(typeof messages.is_ready === 'object'){
                        toastr.error(messages.is_ready[0]);
                    }

                    if(typeof messages.description === 'object'){
                        toastr.error(messages.description[0]);
                    }
                }
            }));
        }
        
        function deleteProduct(id) {
            var c = confirm("Hapus Produk ?");

            if(c){
                $.ajax({
                    url: "/produk/"+id+"/hapus",
                    dataType: "JSON",
                    type: "GET",
                    success:function(resp){
                        toastr.success("Produk dihapus");
                        dtable.ajax.reload();
                    },error(resp){
                        toastr.error("Terjadi kesalahan");
                    }
                })
            }
        }

        modalEditProduct.on("show.bs.modal",function(e){
            var trig = $(e.relatedTarget);
            var modal = $(this);
            var id = trig.data("id");

            $.getJSON("/produk/"+id+"/fetch",function(resp){
                modal.find("#name2").val(resp.name);
                modal.find("#price2").val(resp.price);
                modal.find("#ready2").val(resp.is_ready);
                modal.find("#desc2").val(resp.description);
                modal.find("#kategori2").val(resp.product_category_id);
                modal.find("#id").val(resp.id);
            });
        })


    </script>
@endpush
