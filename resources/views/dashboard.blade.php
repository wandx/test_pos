@extends("master")

@section("page_header","Dashboard")
@push("contents")
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>{{ (new \App\Models\Order())->newQuery()->count() }}</h3>

                    <p>Total Order</p>
                </div>
                <div class="icon">
                    <i class="ion ion-bag"></i>
                </div>
                <a href="{{ route("order") }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-orange">
                <div class="inner">
                    <h3>{{ (new \App\Models\Product())->newQuery()->count() }}</h3>

                    <p>Total Produk</p>
                </div>
                <div class="icon">
                    <i class="ion ion-android-car"></i>
                </div>
                <a href="{{ route("product") }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>{{ (new \App\Models\ProductCategory())->newQuery()->count() }}</h3>

                    <p>Total Kategori</p>
                </div>
                <div class="icon">
                    <i class="ion ion-android-boat"></i>
                </div>
                <a href="{{ route("category") }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-blue">
                <div class="inner">
                    <h3>{{ (new \App\Models\User())->newQuery()->count() }}</h3>

                    <p>Total Pegawai</p>
                </div>
                <div class="icon">
                    <i class="ion ion-android-people"></i>
                </div>
                <a href="{{ route("employee") }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>{{ (new \App\Models\Order())->newQuery()->sum("total") }}</h3>

                    <p>Total Pendapatan</p>
                </div>
                <div class="icon">
                    <i class="ion ion-compass"></i>
                </div>
                <a href="{{ route("order") }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
    </div>

@endpush
