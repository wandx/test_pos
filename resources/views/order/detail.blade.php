<div class="form-horizontal">
    <div class="form-group">
        <label for="" class="control-label col-sm-4">No. Meja</label>
        <div class="col-sm-6">
            <span class="form-control">{{ $order->table_number }}</span>
        </div>
    </div>

    <table class="table">
        <thead>
        <tr>
            <th>Nama</th>
            <th class="text-right">Qty</th>
            <th class="text-right">SubTotal</th>
        </tr>
        </thead>
        <tbody>
        @foreach($order->order_items as $item)
            <tr>
                <td>{{ $item->name }}</td>
                <td class="text-right">{{ $item->qty }}</td>
                <td class="text-right">{{ $item->sub_total }}</td>
            </tr>
        @endforeach

        </tbody>
    </table>
</div>
