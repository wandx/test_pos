@extends("master")

@section("page_header")
    Order
    <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#add-order">Buat Order</button>
@endsection

@push("contents")
    <div class="panel">
        <div class="panel-body">
            <table class="table" id="dtable">
                <thead>
                <tr>
                    <th>Code</th>
                    <th>Status</th>
                    <th>No. Meja</th>
                    <th>Dibuat Oleh</th>
                    <th>Tgl</th>
                    <th>Aksi</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@endpush

@push("contents")
    <!-- Modal -->
    <div id="add-order" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <form method="post" class="modal-content" id="f-add-product" onsubmit="submitOrder(event)">
                {!! csrf_field() !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Buat Order</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-7">
                            <div class="table-responsive">
                                <table id="product-selector" class="table table-borderless">
                                    <thead>
                                    <tr>
                                        <th>Nama</th>
                                        <th>Price</th>
                                        <th>Desc</th>
                                        <th>Kategori</th>
                                        <th>Aksi</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>

                        <div class="col-sm-5">
                            <div class="form-group">
                                <input required type="text" name="table_number" id="no-meja" class="form-control" placeholder="No. Meja">
                            </div>

                            <div class="row-container" style="overflow-y: scroll;overflow-x: hidden;height: 250px">

                            </div>
                            <input type="hidden" name="total">
                            <h3 class="text-right">Total: <span id="total">0</span></h3>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary" type="submit">Submit</button>
                </div>
            </form>
        </div>
    </div>

    <div class="hidden master-item">
        <div class="row cart-item" style="border-bottom: 1px solid black;margin-bottom: 10px;">
            <input type="hidden" class="pid" name="pid[]">
            <input type="hidden" class="pname_input" name="pname_input[]">
            <input type="hidden" class="price_input" name="price_input[]">
            <div class="col-xs-5">
                <span class="pname">Product Name</span> <br>
                <small class="price">0</small>
            </div>
            <div class="col-xs-5">
                <button class="btn btn-xs btn-primary decrement-qty" type="button" onclick="decrementItem(event)"> - </button>
                <input type="text" class="qty" value="1" name="qty[]" style="width: 30px;text-align: center;">
                <button class="btn btn-xs btn-primary increment-qty" type="button" onclick="incrementItem(event)"> + </button>
            </div>
            <div class="col-xs-1">
                <button class="btn btn-xs btn-danger remove-item" type="button" onclick="removeItem(event)">DEL</button>
            </div>
        </div>
    </div>

@endpush

@push("contents")
    <!-- Modal -->
    <div id="order-detail" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Order detail</h4>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <!-- Modal -->
    <div id="payment" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <form class="modal-content form-horizontal" id="f-payment" onsubmit="submitPayment(event)">
                {!! csrf_field() !!}
                <input type="hidden" id="mid" name="id">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Pembayaran</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="" class="control-label col-sm-4">TOTAL</label>
                        <div class="col-sm-7">
                            <span class="form-control" id="m-total">0</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="" class="control-label col-sm-4">DIBAYAR</label>
                        <div class="col-sm-7">
                            <input class="form-control" name="payment_amount" required type="number" id="m-amount" value="0" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="" class="control-label col-sm-4">KEMBALI</label>
                        <div class="col-sm-7">
                            <span class="form-control" id="m-change">0</span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary" type="submit">Submit</button>
                </div>
            </form>

        </div>
    </div>
@endpush

@push("scripts")
    <script>
        var dtable;
        var productSelector;
        var orderForm = $("#f-add-product");
        var addOrderModal = $("#add-order");
        var rowContainer = $(".row-container");
        var orderDetailModal = $("#order-detail");
        var paymentModal = $("#payment");
        var formPayment = $("#f-payment");

        $(function(){
            productSelector = $("#product-selector").DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route("order.product-data") }}",
                columns:[
                    { data: "name",name: "name"},
                    { data: "price",name: "price"},
                    { data: "description",name: "description"},
                    { data: "product_category.name",name: "product_category.name",orderable:false,searchable:false},
                    { data: "action",name: "action",orderable:false,searchable:false},
                ]
            });

            dtable = $("#dtable").DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route("order.data") }}",
                columns:[
                    { data: "code",name: "code"},
                    { data: "status",name: "status"},
                    { data: "table_number",name: "table_number"},
                    { data: "user.name",name: "user.name",orderable:false,searchable:false},
                    { data: "created_at",name: "created_at"},
                    { data: "action",name: "action",orderable:false,searchable:false},
                ],
                order:[
                    [4,"desc"]
                ]
            });
        });

        function addItem(name,price,id) {
            var master = $(".master-item");
            var container = $(".row-container");

            master.find(".pname").html(name);
            master.find(".price").html(price);
            master.find(".pid").val(id);
            master.find(".pname_input").val(name);
            master.find(".price_input").val(price);

            if(!isProductAlreadyExist(id)){
                container.append(master.html());
            }

            updateTotal();
        }

        function incrementItem(e) {
            var currentVal = $(e.target).prev(".qty");
            var newVal = parseInt(currentVal.val()) + 1;
            currentVal.val(newVal);
            updateTotal();
        }

        function decrementItem(e) {
            var currentVal = $(e.target).next(".qty");
            var newVal = parseInt(currentVal.val()) - 1;
            currentVal.val(newVal);

            if(newVal === 0){
                removeItem(e);
            }
            updateTotal();
        }

        function removeItem(e){
            $(e.target).parent().parent().remove();
            updateTotal();
        }

        function isProductAlreadyExist(id) {
            var container = $(".row-container");
            var c = container.find("input[value="+id+"]").length;
            return c !== 0;
        }

        function updateTotal(){
            var container = $(".row-container");
            var total = 0;
            container.find(".cart-item").each(function(idx,elm){
                var price = $(elm).find(".price").html();
                var qty = $(elm).find(".qty").val();
                price = parseInt(price);
                qty = parseInt(qty);

                total += (price*qty);
            });

            $("#total").html(total);
            $("input[name=total]").val(total);
        }

        function submitOrder(e){
            e.preventDefault();
            var data = orderForm.serialize();

            $.ajax({
                url: "{{ route("order.store") }}",
                type: "POST",
                dataType: "JSON",
                data: data,
                success: function(resp){
                    $(document).find("input[name=_token]").val(resp._token);
                    orderForm.trigger("reset");
                    addOrderModal.modal("hide");
                    toastr.success("Order berhasil dibuat.");
                    dtable.ajax.reload();
                    rowContainer.html("");

                }
            })
        }

        orderDetailModal.on("show.bs.modal",function(e){
           var trig = $(e.relatedTarget);
           var modal = $(this);
           var id = trig.data("id");

           $.get("/order/"+id+"/detail",function(resp){
               modal.find(".modal-body").html(resp);
           })
        });

        paymentModal.on("show.bs.modal",function(e){
            var trig = $(e.relatedTarget);
            var modal = $(this);
            var id = trig.data("id");
            var code = trig.data("code");
            var total = trig.data("total");

            modal.find(".modal-title").html("Pembayaran: "+code);
            modal.find("#m-total").html(total);
            modal.find("#m-amount").val(total);
            modal.find("#mid").val(id);
        });

        function updateChange(){
            var total = $("#m-total").html();
            total = parseInt(total);
            var amount = $("#m-amount").val();
            amount = parseInt(amount);

            $("#m-change").html(amount - total);
        }

        $("#m-amount").on("keyup",function(){
            updateChange();
        });


        function submitPayment(e) {
            e.preventDefault();
            var data = formPayment.serialize();

            $.ajax({
                url: "{{ route("order.pay") }}",
                type: "POST",
                dataType: "JSON",
                data: data,
                success: function(resp){
                    $(document).find("input[name=_token]").val(resp._token);
                    paymentModal.modal("hide");
                    formPayment.trigger("reset");
                    toastr.success("Pembayaran berhasil.");
                    dtable.ajax.reload();
                }
            })

        }

        function deleteOrder(id) {
            var c = confirm("Hapus order?");
            if(c){
                $.getJSON("/order/"+id+"/delete",function(resp){
                    toastr.success("Order dihapus.");
                    dtable.ajax.reload();
                });
            }

        }

    </script>
@endpush
