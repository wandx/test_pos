@extends("master")

@section("page_header")
    Log Aktifitas
@endsection

@push("contents")
    <div class="panel">
        <div class="panel-body">
            <table class="table" id="dtable">
                <thead>
                <tr>
                    <th>User</th>
                    <th>Aktifitas</th>
                    <th>Tgl</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@endpush

@push("scripts")
    <script>
        var dtable;

        $(function(){
            dtable = $("#dtable").DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route("activity.data") }}",
                columns:[
                    { data: "user.name",name: "user.name",orderable:false,searchable:false},
                    { data: "desc",name: "desc"},
                    { data: "updated_at",name: "updated_at"},
                ],
                order:[
                    [2,"desc"]
                ]
            });
        });
    </script>
@endpush
