@extends("master")

@section("page_header")
    Pegawai
    <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#add-user">Tambah Pegawai</button>
@endsection

@push("contents")
    <div class="panel">
        <div class="panel-body">
            <table class="table" id="dtable">
                <thead>
                <tr>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Role</th>
                    <th>Aksi</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@endpush

@push("contents")
    <!-- Modal -->
    <div id="add-user" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <form method="post" class="modal-content" id="f-add-user" onsubmit="submitAddUser(event)">
                {!! csrf_field() !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah User</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Nama</label>
                        <input type="text" name="name" id="name" required class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" name="email" id="email" required class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" name="password" id="password" required class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="password_confirmation">Password Confirmation</label>
                        <input type="password" name="password_confirmation" id="password_confirmation" required class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="role">Role</label>
                        <select name="role" id="role" class="form-control">
                            <option value="">Pilih Role</option>
                            <option value="kasir">Kasir</option>
                            <option value="pelayan">Pelayan</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary" type="submit">Submit</button>
                </div>
            </form>

        </div>
    </div>

@endpush

@push("contents")
    <!-- Modal -->
    <div id="edit-user" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <form method="post" class="modal-content" id="f-edit-user" onsubmit="submitEditUser(event)">
                {!! csrf_field() !!}
                <input type="hidden" name="id" id="id">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit User</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name2">Nama</label>
                        <input type="text" name="name" id="name2" required class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="email2">Email</label>
                        <input type="email" name="email" id="email2" required class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="password2">Password</label>
                        <input type="password" name="password" id="password2" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="password_confirmation2">Password Confirmation</label>
                        <input type="password" name="password_confirmation" id="password_confirmation2" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="role2">Role</label>
                        <select name="role" id="role2" class="form-control">
                            <option value="">Pilih Role</option>
                            <option value="kasir">Kasir</option>
                            <option value="pelayan">Pelayan</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary" type="submit">Submit</button>
                </div>
            </form>

        </div>
    </div>

@endpush


@push("scripts")
    <script>
        var dtable;
        var formAddUser = $("#f-add-user");
        var modalAddUser = $("#add-user");
        var formEditUser = $("#f-edit-user");
        var modalEditUser = $("#edit-user");

        $(function(){
            dtable = $("#dtable").DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route("employee.data") }}",
                columns:[
                    { data: "name",name: "name"},
                    { data: "email",name: "email"},
                    { data: "role",name: "role"},
                    { data: "action",name: "action",orderable:false,searchable:false},
                ]
            });
        });

        function submitAddUser(e){
            e.preventDefault();
            var data = formAddUser.serialize();

            $.when($.ajax({
                url: "{{ route("employee.store") }}",
                dataType: "JSON",
                type: "POST",
                data: data,
                success: function(resp){
                    // update token
                    modalAddUser.modal("hide");
                    formAddUser.trigger("reset");
                    formAddUser.find("input[name=_token]").val(resp._token);
                    dtable.ajax.reload();
                    toastr.success("Berhasil menambah pegawai.");

                },
                error: function(resp){
                    var messages = resp.responseJSON.messages;
                    var token = resp.responseJSON._token;

                    // update token
                    formAddUser.find("input[name=_token]").val(token);


                    if(typeof messages.email === 'object'){
                        toastr.error(messages.email[0]);
                    }

                    if(typeof messages.name === 'object'){
                        toastr.error(messages.name[0]);
                    }

                    if(typeof messages.password === 'object'){
                        toastr.error(messages.password[0]);
                    }

                    if(typeof messages.role === 'object'){
                        toastr.error(messages.role[0]);
                    }
                }
            }));
        }

        function submitEditUser(e){
            e.preventDefault();
            var data = formEditUser.serialize();

            $.when($.ajax({
                url: "{{ route("employee.update") }}",
                dataType: "JSON",
                type: "POST",
                data: data,
                success: function(resp){
                    // update token
                    modalEditUser.modal("hide");
                    formEditUser.trigger("reset");
                    formEditUser.find("input[name=_token]").val(resp._token);
                    dtable.ajax.reload();
                    toastr.success("Berhasil update pegawai.");

                },
                error: function(resp){
                    var messages = resp.responseJSON.messages;
                    var token = resp.responseJSON._token;

                    // update token
                    formEditUser.find("input[name=_token]").val(token);


                    if(typeof messages.email === 'object'){
                        toastr.error(messages.email[0]);
                    }

                    if(typeof messages.name === 'object'){
                        toastr.error(messages.name[0]);
                    }

                    if(typeof messages.password === 'object'){
                        toastr.error(messages.password[0]);
                    }

                    if(typeof messages.role === 'object'){
                        toastr.error(messages.role[0]);
                    }
                }
            }));
        }
        
        function deleteUser(id) {
            var c = confirm("Hapus Pegawai ?");

            if(c){
                $.ajax({
                    url: "/pegawai/"+id+"/hapus",
                    dataType: "JSON",
                    type: "GET",
                    success:function(resp){
                        toastr.success("Pegawai dihapus");
                        dtable.ajax.reload();
                    },error(resp){
                        toastr.error("Terjadi kesalahan");
                    }
                })
            }
        }

        modalEditUser.on("show.bs.modal",function(e){
            var trig = $(e.relatedTarget);
            var modal = $(this);
            var id = trig.data("id");

            $.getJSON("/pegawai/"+id+"/fetch",function(resp){
               modal.find("#name2").val(resp.name);
               modal.find("#email2").val(resp.email);
               modal.find("#role2").val(resp.role);
               modal.find("#id").val(resp.id);
            });
        })


    </script>
@endpush
