<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware("guest")->get("/","LoginCont@index")->name("login");
Route::middleware("guest")->post("/","LoginCont@doLogin")->name("login.post");

Route::group(["middleware"=>"auth"],function(){
    Route::get("dashboard","DashboardCont@index")->name("dashboard");
    Route::get("logout","LoginCont@doLogout")->name("logout");

    Route::group(["middleware"=>"for:admin"],function(){
        Route::get("pegawai","EmployeeCont@index")->name("employee");
        Route::get("pegawai-data","EmployeeCont@data")->name("employee.data");
        Route::post("pegawai","EmployeeCont@store")->name("employee.store");
        Route::post("pegawai/update","EmployeeCont@update")->name("employee.update");
        Route::get("pegawai/{id}/hapus","EmployeeCont@destroy")->name("employee.destroy");
        Route::get("pegawai/{id}/fetch","EmployeeCont@fetch")->name("employee.fetch");

        Route::get("activity","ActivityCont@index")->name("activity");
        Route::get("activity-data","ActivityCont@data")->name("activity.data");
    });


    Route::get("produk","ProductCont@index")->name("product");
    Route::get("produk-data","ProductCont@data")->name("product.data");
    Route::middleware("for:admin")->post("produk","ProductCont@store")->name("product.store");
    Route::middleware("for:admin")->post("produk/update","ProductCont@update")->name("product.update");
    Route::middleware("for:admin")->get("produk/{id}/hapus","ProductCont@destroy")->name("product.destroy");
    Route::middleware("for:admin")->get("produk/{id}/fetch","ProductCont@fetch")->name("product.fetch");

    Route::get("kategori","CategoryCont@index")->name("category");
    Route::get("kategori-data","CategoryCont@data")->name("category.data");
    Route::middleware("for:admin")->post("kategori","CategoryCont@store")->name("category.store");
    Route::middleware("for:admin")->post("kategori/update","CategoryCont@update")->name("category.update");
    Route::middleware("for:admin")->get("kategori/{id}/hapus","CategoryCont@destroy")->name("category.destroy");
    Route::middleware("for:admin")->get("kategori/{id}/fetch","CategoryCont@fetch")->name("category.fetch");

    Route::get("order","OrderCont@index")->name("order");
    Route::get("order-data","OrderCont@data")->name("order.data");
    Route::get("order/product-data","OrderCont@dataProduct")->name("order.product-data");
    Route::post("order/store","OrderCont@store")->name("order.store");
    Route::get("order/{id}/detail","OrderCont@orderDetail")->name("order.detail");
    Route::post("order/pay","OrderCont@pay")->name("order.pay");
    Route::get("order/{id}/delete","OrderCont@destroy")->name("order.delete");

});
