<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(["namespace"=>"Api"],function(){
    Route::post("login","AuthCont@login");

    Route::group(["middleware"=>"api-auth"],function(){
       Route::post("logout","AuthCont@logout");
       Route::post("refresh","AuthCont@refresh");
       Route::get("me","AuthCont@me");

       Route::get("produk","ProductCont@index");
    });
});
